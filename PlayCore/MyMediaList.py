from enum import Enum
from PyQt5 import QtCore
from PyQt5.QtCore import QObject,QUrl,QTime
from tablewidgets.MyTablePlayListFinal import *
class PlayMode(Enum):
    PlayInOrder = 0
    PlayRandom = 1
    PlayOneCircle = 2

class MyMediaList(QObject):
    def __init__(self,parent=None):
        super(MyMediaList, self).__init__(parent)
        self.Final = None
        self.musicIndex = 0
        self.list = []
        self.hashMap = {}
        self.indexMode = 0

        self.setPlayMode(PlayMode.PlayInOrder)
    def mediaUrl(self,index):
        if len(self.list) == 0:
            return QUrl("")
        self.musicIndex = index
        return self.list[index]
    def setPlayMode(self,mode):
        if mode == PlayMode.PlayInOrder:
            self.indexMode = 0
        elif mode == PlayMode.PlayRandom:
            self.indexMode = 1
        elif mode == PlayMode.PlayOneCircle:
            self.indexMode = 2

    def nextMediaIndex(self):
        if self.indexMode == 0:
            if self.musicIndex+1 >= len(self.list):
                self.musicIndex = 0
            else:
                self.musicIndex+=1
        elif self.indexMode == 1:
            time = QTime.currentTime()
            QtCore.qsrand(time.msec()+time.second()*1000)
            xxx = QtCore.qrand()%len(self.list)
            self.musicIndex=xxx
        return self.musicIndex
    def preMediaIndex(self):
        if self.indexMode == 0:
            if self.musicIndex == 0:
                self.musicIndex = 0
            else:
                self.musicIndex-=1
        elif self.indexMode == 1:
            time = QTime.currentTime()
            QtCore.qsrand(time.msec()+time.second()*1000)
            xxx = QtCore.qrand()%len(self.list)
            self.musicIndex=xxx
        return self.musicIndex
    def slot_removeSong(self,index):
        del self.hashMap[self.list[index]]
        del self.list[index]
        playwidindex = self.Final.table.m_playingWid.currentSongIndex()
        if playwidindex >= index:
            self.musicIndex-=1













