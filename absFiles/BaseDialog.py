from PyQt5 import QtCore,QtGui,QtWidgets
from PyQt5.QtWidgets import QDialog,QSizePolicy,QGridLayout
from absFiles.BaseWindow import *

class BaseDialog(QDialog):
    def __init__(self,parent=None):
        super(BaseDialog, self).__init__(parent)
        self.mainwid = Widget(self)
        self.MousePressed = False
        self.isPressBorder = False
        self.MousePos = QPoint()
        self.WinodwPos = QPoint()
        self.setMinimumSize(550,440)
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint|QtCore.Qt.Tool|QtCore.Qt.X11BypassWindowManagerHint)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground,True)

        self.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)

        self.mainwid.setAutoFillBackground(True)
        layout = QGridLayout()
        layout.addWidget(self.mainwid)
        layout.setContentsMargins(4,4,4,4)
        self.setLayout(layout)


    def paintEvent(self, a0: QtGui.QPaintEvent):
        super(BaseDialog, self).paintEvent(a0)
        path = QPainterPath()
        path.setFillRule(QtCore.Qt.WindingFill)
        path.addRect(9,9,self.width()-18,self.height() - 18)

        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing,True)
        painter.fillPath(path,QBrush(QtCore.Qt.white))


        color = QColor(0,0,0,50)
        for i in range(9):
            path = QPainterPath()
            path.setFillRule(QtCore.Qt.WindingFill)
            path.addRect(9-i,9-i,self.width()-(9-i)*2,self.height()-(9-i)*2)
            color.setAlpha(150-math.sqrt(i)*50)
            painter.setPen(color)
            painter.drawPath(path)
    def mousePressEvent(self, a0: QtGui.QMouseEvent):
        self.isPressBorder = False
        self.setFocus()
        if a0.button() == QtCore.Qt.LeftButton:
            rect = QRect(4,4,self.width() - 8,self.height()-8)
            if rect.contains(a0.pos()):
                self.WinodwPos = self.pos()
                self.MousePos = a0.globalPos()
                self.MousePressed = True
            else:
                self.isPressBorder = True

    def mouseMoveEvent(self, a0: QtGui.QMouseEvent):
        if self.MousePressed:
            self.move(self.WinodwPos + (a0.globalPos() - self.MousePos))

    def mouseReleaseEvent(self, a0: QtGui.QMouseEvent):
        self.isPressBorder = False
        if a0.button() == QtCore.Qt.LeftButton:
            self.MousePressed = False



















