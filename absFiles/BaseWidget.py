from PyQt5 import QtCore,QtGui,QtWidgets
from PyQt5.QtCore import QPoint,QRect
from PyQt5.QtWidgets import QWidget,QStyleOption,QStyle
from PyQt5.QtGui import QPainter


class BaseWidget(QWidget):
    def __init__(self,parent=None):
        super(BaseWidget, self).__init__(parent)
        self.setStyleSheet("QWidget{background:transparent;}")
        self.setMouseTracking(True)

    def paintEvent(self, a0: QtGui.QPaintEvent):
        opt = QStyleOption()
        opt.initFrom(self)
        p = QPainter(self)
        self.style().drawPrimitive(QStyle.PE_Widget,opt,p,self)
    def mousePressEvent(self, a0: QtGui.QMouseEvent):
        super(BaseWidget, self).mousePressEvent(a0)
    def mouseMoveEvent(self, a0: QtGui.QMouseEvent):
        #if (mainWindow: :
        #    GetInstance())
        super(BaseWidget, self).mouseMoveEvent(a0)





























