from PyQt5 import QtCore,QtGui,QtWidgets
from PyQt5.QtWidgets import QDialog,QSizePolicy
from PyQt5.QtWidgets import QWidget,QGridLayout
from PyQt5.QtGui import QPixmap,QPainter,QPainterPath,QBrush
from absFiles.AbsFrameLessAutoSize import *
import math

class Widget(QWidget):
    def __init__(self,parent=None):
        super(Widget, self).__init__(parent)
        self.bShowSinger = False
        self.isShowSingerBG = True
        self.skinPic = QPixmap("")
        self.netPic = QPixmap("")
        self.curPix = QPixmap("")
        self.curPixPath = ""


    def setCurBGPic(self,strPix):
        self.netPic.load(strPix)
        self.bShowSinger = True
        self.update()

    def setShowSingerBG(self,ishow):
        self.bShowSinger = ishow

    def clearBg(self):
        self.bShowSinger = False
        self.netPic.load("")
        self.update()

    def setKin(self,skin):
        self.skinPic.load(skin)
        self.curPixPath = skin
        self.update()

    def paintEvent(self, a0: QtGui.QPaintEvent):
        painter = QPainter(self)
        if not self.bShowSinger:
            d = self.skinPic.height()/self.skinPic.width()
            h = d * self.width()
            w = self.height()/d
            if h < self.height():
                painter.drawPixmap(0,0,w,self.height(),self.skinPic)
                return
            painter.drawPixmap(0,0,self.width(),h,self.skinPic)
        if  self.bShowSinger and self.isShowSingerBG:
            d = self.netPic.height()/self.netPic.width()
            h = d*self.width()
            w = self.height()/d
            if h < self.height():
                painter.drawPixmap(0,0,w,self.height(),self.netPic)
                return
            painter.drawPixmap(0,0,self.width(),h,self.netPic)



class BaseWindow(AbsFrameLessAutoSize):
    def __init__(self,parent=None):
        self.border = 8
        self.setMinimumSize(550,440)
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground,True)

        self.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)
        self.mainwid = Widget()
        self.mainwid.setAutoFillBackground(True)
        layout = QGridLayout()
        layout.addWidget(self.mainwid)
        layout.setContentsMargins(4,4,4,4)
        self.setLayout(layout)

    def paintEvent(self, a0: QtGui.QPaintEvent):
        super(BaseWindow, self).paintEvent(a0)
        painter = QPainter(self)
        path = QPainterPath()
        path.setFillRule(QtCore.Qt.WindingFill)
        painter.setRenderHint(QPainter.Antialiasing,True)
        painter.fillPath(path,QBrush(QtCore.Qt.white))

        color = QColor(0,0,0,50)
        for i in range(9):
            path = QPainterPath()
            path.setFillRule(QtCore.Qt.WindingFill)
            path.addRect(9-i,9-i,self.width()-(9-i)*2,self.height()-(9-i)*2)
            color.setAlpha(150-math.sqrt(i)*50)
            painter.setPen(color)
            painter.drawPath(path)













