from PyQt5 import QtCore,QtGui,QtWidgets
from PyQt5.QtCore import QObject,QTimer,QRect
from PyQt5.QtWidgets import QWidget,QSlider,QPushButton,QToolTip
from PyQt5.QtGui import QMouseEvent,QPixmap,QPainter,QCursor,QColor,QFont,QFontMetrics
from enum import Enum

class MyPushButton(QPushButton):
    def __init__(self,parent=None):
        super(MyPushButton, self).__init__(parent)
        self.setCursor(QtCore.Qt.PointingHandCursor)
        self.setFlat(True)
        self.setStyleSheet("QPushButton{background:white;}")


class VolButton(QPushButton):
    setMute = QtCore.pyqtSignal(int)
    def __init__(self,normal,hover,pressed,parent=None):
        super(VolButton, self).__init__(parent)
        self.partnerslider = None
        self.isenter = False
        self.index = 0
        self.isvolempty = 100
        self.savevol = 100
        self.setCursor(QtCore.Qt.PointingHandCursor)

        pixTemp = QPixmap(normal)
        self.listnormal = []
        self.listhover = []
        self.listpressed = []
        for i in range(5):
            self.listnormal.append(pixTemp.copy(i*(pixTemp.width()/5),0,pixTemp.width()/5,pixTemp.height()))
        pixTemp.load(hover)
        for i in range(5):
            self.listhover.append(pixTemp.copy(i*(pixTemp.width()/5),0,pixTemp.width()/5,pixTemp.height()))
        pixTemp.load(pressed)
        for i in range(5):
            self.listhover.append(pixTemp.copy(i * (pixTemp.width() / 5), 0, pixTemp.width() / 5, pixTemp.height()))


    def paintEvent(self, a0: QtGui.QPaintEvent):
        p = QPainter(self)
        if self.isenter:
            p.drawPixmap((self.width()-self.listnormal[0].width())/2,(self.height()-self.listnormal[0].height())/2,self.listnormal[self.index])
        else:
            p.drawPixmap((self.width() - self.listhover[0].width())/2,(self.height()-self.listhover[0].height())/2,self.listhover[self.index])

    def mousePressEvent(self, e: QtGui.QMouseEvent):
        if e.button() == QtCore.Qt.LeftButton:
            super(VolButton, self).mousePressEvent(e)

    def mouseReleaseEvent(self, e: QtGui.QMouseEvent):
        if e.button() == QtCore.Qt.LeftButton:
            if self.contentsRect().contains(self.mapFromGlobal(QCursor.pos())):
                if self.isvolempty == 0:
                    self.setMute.emit(self.savevol)
                else:
                    self.savevol = self.partnerslider.value()
                    self.setMute.emit(self.savevol)
            super(VolButton, self).mouseReleaseEvent(e)
    def setButtonPixmap(self,value):
        self.isenter = False
        if value == 0:
            self.index = 4
        if value > 3 and value <= 30:
            self.index = 1
        if value>30:
            self.index = 2
        self.update()
        self.isvolempty = value
    def enterEvent(self, a0: QtCore.QEvent):
        self.isenter = True
        self.update()
    def leaveEvent(self, a0: QtCore.QEvent):
        self.isenter = False
        self.update()
class VolButton2(VolButton):
    sig_hideVolWidget = QtCore.pyqtSignal()
    def __init__(self,normal,hover,pressed,parent=None):
        super(VolButton2, self).__init__(normal,hover,pressed,parent)
        self.timer = QTimer()
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.sig_hideVolWidget)

    def enterEvent(self, a0: QtCore.QEvent):
        self.isenter = True
        self.timer.stop()
        self.update()
    def leaveEvent(self, a0: QtCore.QEvent):
        self.isenter = False
        self.timer.start(500)
        self.update()


class stackButton(QPushButton):
    def __init__(self,pixnormal,pixhover,pixsel,parent):
        super(stackButton, self).__init__(parent)
        self.pixnormal = pixnormal
        self.pixhover = pixhover
        self.pixselected = pixsel
        self.index = -1
        self.isenter = False
        self.pressed = False

        self.setCursor(QtCore.Qt.PointingHandCursor)
        self.setFlat(True)

    def paintEvent(self, a0: QtGui.QPaintEvent):
        super(stackButton, self).paintEvent(a0)
        p = QPainter(self)
        if not self.isenter and not self.pressed:
            p.drawPixmap((self.width() - self.pixnormal.width()) / 2,
                         (self.height() - self.pixnormal.height()) / 2, self.pixnormal)
        elif self.isenter and not self.pressed:
            p.drawPixmap((self.width() - self.pixhover.width()) / 2,
                         (self.height() - self.pixhover.height()) / 2, self.pixhover)
        elif self.pressed:
            p.drawPixmap((self.width() - self.pixselected.width()) / 2,
                         (self.height() - self.pixselected.height()) / 2, self.pixselected)

    def setselected(self,sel):
        self.pressed = sel
        self.update()

    def mousePressEvent(self, e: QtGui.QMouseEvent):
        super(stackButton, self).mousePressEvent(e)
        if e.button() == QtCore.Qt.LeftButton:
            self.pressed = True
            self.update()

    def enterEvent(self, a0: QtCore.QEvent):
        super(stackButton, self).enterEvent(a0)
        self.isenter = True
        self.update()
    def leaveEvent(self, a0: QtCore.QEvent):
        super(stackButton, self).leaveEvent(a0)
        self.isenter = False
        self.update()

class LeftPixButton(QPushButton):
    def __init__(self,pixnormal,pixhover,parent=None):
        super(LeftPixButton, self).__init__(parent)
        self.isenter = False
        self.pressed = False
        self.pixnormal = QPixmap(pixnormal)
        self.pixhover = QPixmap(pixhover)

    def paintEvent(self, a0: QtGui.QPaintEvent):
        p = QPainter(self)
        if not self.isenter:
            p.drawPixmap(44,(self.height() - self.pixnormal.height())/2,self.pixnormal)
        elif self.isenter:
            p.drawPixmap(44,(self.height()-self.pixhover.height())/2,self.pixhover)

    def mousePressEvent(self, e: QtGui.QMouseEvent):
        if e.button() == QtCore.Qt.LeftButton:
            self.isenter = True
            self.update()
        super(LeftPixButton, self).mousePressEvent(e)
    def enterEvent(self, a0: QtCore.QEvent):
        self.isenter = True
        self.update()

    def leaveEvent(self, a0: QtCore.QEvent):
        self.isenter = False
        self.update()


class PlayMode(Enum):
    PlayInOrder = 0
    PlayRandom=1
    PlayOnCircle=2


class PlayModeButton(QPushButton):
    def __init__(self,pixnormal,pixhover,text,parent=None):
        self.isenter = False
        self.pressed = False
        self.mode = PlayMode.PlayInOrder
        self.pixnormal = QPixmap(pixnormal)
        self.pixhover = QPixmap(pixhover)
        self.text = text

        self.setStyleSheet("background:transparent;")
        self.setFixedSize(100,20)
        self.setCheckBtn(False)

    def enterEvent(self, a0: QtCore.QEvent):
        super(PlayModeButton, self).enterEvent(a0)
        self.isenter = True
        self.update()
    def mousePressEvent(self, e: QtGui.QMouseEvent):
        super(PlayModeButton, self).mousePressEvent(e)
        self.pressed = True
        self.update()

    def leaveEvent(self, a0: QtCore.QEvent):
        super(PlayModeButton, self).leaveEvent(a0)
        self.isenter = False
        self.update()

    def paintEvent(self, a0: QtGui.QPaintEvent):
        super(PlayModeButton, self).paintEvent(a0)
        p = QPainter(self)
        p.setPen(QtCore.Qt.gray)
        p.drawPixmap(10,2,self.pixnormal)
        p.drawText(35,15,self.text)
        if self.pressed or self.isenter:
            p.setPen(QtCore.Qt.white)
            p.drawPixmap(10,2,self.pixhover)
            p.drawText(35,15,self.text)
    def setCheckBtn(self,check):
        self.pressed = check
        self.update()

class PlayingWidgetBtn(MyPushButton):
    def __init__(self,parent=None):
        super(PlayingWidgetBtn, self).__init__(parent)
        self.text = ""
        self.normalcolor = QColor(0,0,0)
        self.hovercolor = QColor(0,0,0,)
        self.isenter = False

        self.setCursor(QtCore.Qt.ArrowCursor)
        self.setMouseTracking(True)

    def paintEvent(self, a0: QtGui.QPaintEvent):
        p = QPainter(self)
        p.setPen(self.normalcolor)
        metrics = QFontMetrics(self.font())

        if self.isenter:
            p.setPen(self.hovercolor)
            f = QFont()
            f.setUnderline(True)
            p.setFont(f)
        p.drawText(0,(self.height() - metrics.height())/2,self.width(),metrics.height(),QtCore.Qt.AlignCenter,self.text)
    def mouseMoveEvent(self, e: QtGui.QMouseEvent):
        super(PlayingWidgetBtn, self).mouseMoveEvent(e)
        metrics = QFontMetrics()
        re = QRect((self.width()-metrics.width(self.text))/2,(self.height()-metrics.height())/2,metrics.width(self.text),metrics.height())

        if re.contains(e.pos()):
            self.isenter = True
            QToolTip.showText(QCursor.pos(),self.text)
            self.setCursor(QtCore.Qt.PointingHandCursor)
        else:
            self.isenter = False
            self.setCursor(QtCore.Qt.ArrowCursor)
        self.update()
    def mousePressEvent(self, e: QtGui.QMouseEvent):
        if self.isenter:
            super(PlayingWidgetBtn, self).mousePressEvent(e)
    def leaveEvent(self, a0: QtCore.QEvent):
        super(PlayingWidgetBtn, self).leaveEvent(a0)
        self.isenter = False

    def setNormalColor(self,color):
        self.normalcolor=color

    def adjustSize(self):
        super(PlayingWidgetBtn, self).adjustSize()
        metrics = QFontMetrics(self.font())
        fw = metrics.width(self.text)
        if fw > self.maximumWidth():
            fw = self.maximumWidth()
        self.setFixedWidth(fw)

    def setText(self,text):
        self.text = text
        self.update()
        




















