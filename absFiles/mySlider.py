from PyQt5 import QtCore,QtGui,QtWidgets
from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import QWidget,QSlider
from PyQt5.QtGui import QMouseEvent


class MySlider(QSlider):
    def __init__(self,orientation,parent=None):
        super(MySlider, self).__init__(orientation,parent)
        self.setMaximum(100)
        self.setMinimum(0)
        self.setCursor(QtCore.Qt.PointingHandCursor)
        self.setMouseTracking(True)

    def mousePressEvent(self, ev: QtGui.QMouseEvent):
        if ev.button() == QtCore.Qt.LeftButton:
            dur = self.maximum() - self.minimum()
            pos = 0
            if self.orientation() == QtCore.Qt.Vertical:
                pos = self.maximum() - dur *(ev.y()/self.height())
            else:
                pos = self.minimum()+dur*(ev.x()/self.width())
            if pos != self.sliderPosition():
                self.setValue(pos)

        super(MySlider, self).mousePressEvent(ev)


























