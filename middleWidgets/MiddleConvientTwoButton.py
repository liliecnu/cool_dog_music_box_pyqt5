from PyQt5.QtCore import QObject,QTimer
from PyQt5.QtWidgets import QWidget,QLineEdit,QHBoxLayout
from PyQt5.QtGui import QColor
from absFiles.BaseWidget import *
from absFiles.myPushButton import *
from res.res_bottomwidget import *
from res.res_main import *
from res.res_middlewidget import *
from res.res_otherwidget import *
from res.res_topwidget import *
from res.res_traymenu import *

class MiddleConvientTwoButton(BaseWidget):
    def __init__(self,parent=None):
        super(MiddleConvientTwoButton, self).__init__(parent)

        self.btnsearch = MyPushButton(self)
        self.btnlocate = MyPushButton(self)

        self.btnsearch.setGeometry(5,5,24,24)
        self.btnlocate.setGeometry(35,5,24,24)

        self.setFixedSize(65,34)
        self.setStyleSheet("baseWidget{background: rgb(220,220,220,150);border:1px; border-radius: 17px;}")
        self.btnsearch.setStyleSheet("QPushButton{border-image:url(:/image/middlewidget/btn_search (1).png)}"
                                "QPushButton:hover{border-image:url(:/image/middlewidget/btn_search (2).png)}"
                                "QPushButton:pressed{border-image:url(:/image/middlewidget/btn_search (3).png)}")
        self.btnlocate.setStyleSheet("QPushButton{border-image:url(:/image/middlewidget/btn_location (1).png)}"
                                "QPushButton:hover{border-image:url(:/image/middlewidget/btn_location (2).png)}"
                                "QPushButton:pressed{border-image:url(:/image/middlewidget/btn_location (3).png)}")

        self.timer = QTimer()
        self.timer.timeout.connect(self.slot_timerEvent)
    def slot_timerEvent(self):
        self.hide()

    def showEvent(self, e,QShowEvent):
        super(MiddleConvientTwoButton, self).showEvent(e)
        self.timer.start(5000)

    def enterEvent(self, e,QEvent):
        self.timer.stop()
    def leaveEvent(self, e,QEvent):
        self.timer.start(5000)


class MiddleListSearch(BaseWidget):
    def __init__(self,parent=None):
        super(MiddleListSearch, self).__init__(parent)
        self.lineEdit = QLineEdit(self)
        self.btnClose = MyPushButton(self)

        self.setFixedHeight(32)
        self.setStyleSheet("QWidget{background: rgb(180,180,180,180);}")

        self.lineEdit.setPlaceholderText("查找所有列表歌曲")
        self.setMinimumWidth(260)
        self.setFixedHeight(22)
        self.btnClose.setFixedSize(20,20)

        self.lineEdit.setStyleSheet("QLineEdit{border-radius:11px;background:rgb(255,255,255,180);}")
        self.btnClose.setStyleSheet("QPushButton{background:transparent; border-image:url(:/image/topwidget/btn_close (1).png);}"
                              "QPushButton::hover{border-image:url(:/image/topwidget/btn_close (2).png);}"
                              "QPushButton::pressed{border-image:url(:/image/topwidget/btn_close (3).png);}")

        hlayout = QHBoxLayout()
        hlayout.addWidget(self.lineEdit)
        hlayout.addWidget(self.btnClose)
        hlayout.setContentsMargins(10,5,10,5)
        self.setLayout(hlayout)
        self.btnClose.clicked.connect(self.hide)
        self.btnClose.clicked.connect(self.setClear)
    def setClear(self):
        self.lineEdit.setText("")
