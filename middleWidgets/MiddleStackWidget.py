from PyQt5 import QtCore,QtGui,QtWidgets
from PyQt5.QtCore import QPoint,QRect
from PyQt5.QtWidgets import QWidget,QScrollBar,QLabel,QVBoxLayout,QLineEdit,QSizePolicy,QHBoxLayout,QSpacerItem,QScrollArea,QFrame
from PyQt5.QtGui import QPainter
from absFiles.BaseWidget import *
from absFiles.myPushButton import *
from topwidget.topSearchTipsWidget import *
from middleWidgets.MiddleConvientTwoButton import *
from mainWindows.MainWindowContentWidget import *
from tablewidgets.MyTablePlayListFinal import *
class MyScrollArea(QScrollArea):
    def __init__(self,parent=None):
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff);
        self.setVerticalScrollBarPolicy(QtCore.QtScrollBarAsNeeded);
        self.setFrameShadow(QFrame.Plain);
        self.setFrameShape(QFrame.NoFrame);

        self.verticalScrollBar().setStyleSheet("QScrollBar{background:transparent; width:10px; padding-right: 2px;}"
                                           "QScrollBar::handle{background:rgb(180,180,180,150);border-radius:4px;}"
                                           "QScrollBar::handle:hover{background: rgb(150,150,150,150);}"
                                           "QScrollBar::add-line:vertical{border:1px rgb(230,230,230,150);height: 1px;}"
                                           "QScrollBar::sub-line:vertical{border:1px rgb(230,230,230,150);height: 1px;}"
                                           "QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {background:transparent;}");

    def mousePressEvent(self, a0: QtGui.QMouseEvent):
        self.setFocus()


class MiddleLeftStackWidget0(MyScrollArea):
    def __init__(self,parent=None):
        super(MiddleLeftStackWidget0, self).__init__(parent)
        self.wid = BaseWidget(self)
        self.convientSTBtn = MyPushButton(self)
        self.searchwid = MiddleListSearch(self)
        self.convtwowid = MiddleConvientTwoButton(self)
        self.addTips = AddLoveListTips()
        self.defaultList = MyTablePlayListFinal(self)
        self.lovedList = MyTablePlayListFinal(self)
        self.vlyout = QVBoxLayout()
        self.vlist = []

        self.setMinimumWidth(310)
        self.setMaximumWidth(380)
        self.init()
        self.initAddTips()
        self.initConvienWidget()
        self.initConnection()

    def slot_setlabelpic(self,strpath,strname):
        PlayList = MyTablePlayListFinal.getCurrentList()

        if PlayList is None:
            return
        if strname == PlayList.currentMusicName():
            PlayList.setCurrentSongAlbumPic(strpath)

    def initAddTips(self):
        self.addTips.hide()

    def initConvientWidget(self):
        self.convtwowid.hide()
        self.convtwowid.btnlocate.clicked.connect(self.scrolltoCurrentPlayList)
        self.searchwid.hide()
        self.convtwowid.btnsearch.clicked.connect(self.searchwid.show)
        self.searchwid.lineEdit.textChanged.connect(self.slot_localSearch)
        self.convientSTBtn.clicked.connect(self.convientSTBtn.hide())
        self.convientSTBtn.setTipsStyle(True)
        self.convientSTBtn.hide()

    def initConnection(self):
        self.verticalScrollBar().valueChanged.connect(self.slot_verScrBarChange)

    def scrolltoCurrentPlayList(self):
        PlayList = MyTablePlayListFinal.getCurrentList()
        if PlayList is None:
            return
        idx = self.vlyout.indexOf(PlayList)
        if not PlayList.table.isHidden() and PlayList.table.rowCount()!=0:
            height = 0
            for i in range(idx):
                height+= self.vlist[i].height()
            index = PlayList.table.currentSongIndex()
            self.verticalScrollBar().setValue(height + index*32)
        elif PlayList.table.isHidden() and PlayList.table.rowCount()!=0:
            PlayList.Btntable.clicked()
            index = PlayList.table.currentSongIndex()
            self.verticalScrollBar().setValue(index*32)
