from PyQt5 import QtCore,QtGui,QtWidgets
from PyQt5.QtCore import QPoint,QRect,QPropertyAnimation
from PyQt5.QtWidgets import QWidget,QFrame,QVBoxLayout,QStackedWidget,QLabel,QLineEdit,QSizePolicy,QHBoxLayout,QSpacerItem
from PyQt5.QtGui import QPainter,QPixmap,QColor,QResizeEvent
from absFiles.BaseWidget import *
from absFiles.myPushButton import *

class MiddleWidgetLeft(BaseWidget):
    color = QColor(230,230,230)
    bgcolor = QColor(255,255,255)
    def __init__(self,parent=None):
        super(MiddleWidgetLeft, self).__init__(parent)
        self.widget0 = None
        self.widget1 = None
        self.widget2 = None
        self.widget3 = None
        self.widget4 = None
        self.aanimation = QPropertyAnimation(self,"m_x")
        self.widanimation = QPropertyAnimation(self,"geometry")
        self.stackwid = QStackedWidget()
        self.isDrawVerticalLine = True

        self.setMinimumWidth(310);
        self.setMaximumWidth(380);
        self.setMinimumHeight(570);
        self.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Expanding);

        self.initLayout();
        self.initAnimation();

    def initAnimation(self):
        self.pix = QPixmap(":/image/middlewidget/indicator.png")
        self.index = -1
        self.preindex = -1
        self.preitem = 0
        self.m_x = 0
        self.isAnima = True
        self.aanimation.setDuration(200)
        self.stackwid.setCurrentIndex(0)
        self.aanimation.finished.connect(self.slot_finished)

        self.widanimation.setTargetObject(self.stackwid)
        self.widanimation.setDuration(200)
    def setBackgroundTransparent(self):
        MiddleWidgetLeft.color = QColor(55,55,55,55)
        self.widget0.updateBGColor()
        self.setDrawVerticalLine(False)
        self.update()

    def setBackgroundNormal(self):
        MiddleWidgetLeft.color = QColor(230,230,230)
        self.widget0.updateBGColor()
        self.setDrawVerticalLine(True)
        self.update()

    def setWidgetOpacity(self,opacity):
        MiddleWidgetLeft.bgcolor = QColor(255,255,255,opacity)
        self.update()

    def initLayout(self):
        vlayout = QVBoxLayout()
        self.stackwid.setFrameShadow(QFrame.Plain)
        self.stackwid.setFrameShape(QFrame.NoFrame)

        self.stackwid.addWidget(self.widget0)
        self.stackwid.addWidget(self.widget1)
        self.stackwid.addWidget(self.widget2)
        self.stackwid.addWidget(self.widget3)
        self.stackwid.addWidget(self.widget4)

        self.stackwid.setContentsMargins(0,0,0,0)

        self.btns = []
        self.btns.append(stackButton())
        self.btns.append(stackButton(":/image/middlewidget/btn_music (1).png", ":/image/middlewidget/btn_music (2).png",
                    ":/image/middlewidget/btn_music (3).png", self))
        self.btns.append(stackButton(":/image/middlewidget/btn_cloud (1).png", ":/image/middlewidget/btn_cloud (2).png",
                    ":/image/middlewidget/btn_cloud (3).png", self))
        self.btns.append(stackButton(":/image/middlewidget/btn_radio (1).png", ":/image/middlewidget/btn_radio (2).png",
                    ":/image/middlewidget/btn_radio (3).png", self))
        self.btns.append(stackButton(":/image/middlewidget/btn_phone (1).png", ":/image/middlewidget/btn_phone (2).png",
                    ":/image/middlewidget/btn_phone (3).png", self))
        self.btns.append(stackButton(":/image/middlewidget/btn_download (1).png", ":/image/middlewidget/btn_download (2).png",
                    ":/image/middlewidget/btn_download (3).png", self))

        for i in range(5):
            self.btns[i].setFixedHeight(40)

        hlayout = QHBoxLayout()
        for i in range(5):
            hlayout.addWidget(self.btns[i])
        hlayout.setContentsMargins(0,0,0,0)
        hlayout.setSpacing(0)

        vlayout.addLayout(hlayout)
        vlayout.addWidget(self.stackwid)
        vlayout.setSpacing(0)
        vlayout.setContentsMargins(0,0,0,0)
        self.setLayout(vlayout)

        for i in range(5):
            self.btns[i].setObjectName(str(i))
            self.btns[i].pressed.connect(self.slot_btns)
        self.stackwid.currentChanged.connect(self.slot_changeButtonSelected)

        self.stackwid.setCurrentIndex(0)
        self.btns[0].setselected(True)
    def slot_changeButtonSelected(self,index):
        self.isAnima = True
        self.index = index
        self.aanimation.setStartValue(self.preindex*self.btns[0].widh())
        self.aanimation.setEndValue(self.index*self.btns[0].width())
        self.aanimation.start()

        for i in range(5):
            if i == self.index:
                self.btns[index].setselected(True)
            else:
                self.btns[i].setselected(False)

        if self.index > self.preitem:
            self.widanimation.setTargetObject(self.stackwid)
            self.widanimation.setStartValue(QRect(self.stackwid.width(),40,self.stackwid.width(),self.stackwid.height()))
            self.widanimation.setEndValue(QRect(0,40,self.stackwid.width(),self.stackwid.height()))
            self.widanimation.start()
        if self.index < self.preitem:
            self.widanimation.setTargetObject(self.stackwid)
            self.widanimation.setStartValue(
                QRect(-self.stackwid.width(), 40, self.stackwid.width(), self.stackwid.height()))
            self.widanimation.setEndValue(QRect(0, 40, self.stackwid.width(), self.stackwid.height()))
            self.widanimation.start()
        self.preitem = index

    def paintEvent(self, a0: QtGui.QPaintEvent):
        p = QPainter(self)
        p.setPen(QtCore.Qt.transparent)
        p.setBrush(MiddleWidgetLeft.bgcolor)
        p.drawRect(-1,-1,self.width(),self.height()+1)
        p.setPen(MiddleWidgetLeft.color)

        if self.isAnima:
            if self.isDrawVerticalLine:
                p.drawLine(self.width()-1,0,self.width()-1,self.height())
            p.drawLine(0, self.btns[0].y() + self.btns[0].height() - 1,
                       self.m_x + (self.btns[0].width() - self.pix.width()) / 2 - 1,
                       self.btns[0].y() + self.btns[0].height() - 1);
            p.drawLine(self.m_x + (self.btns[0].width() - self.pix.width()) / 2 + self.pix.width(),
                       self.btns[0].y() + self.btns[0].height() - 1, self.width(),
                       self.btns[0].y() + self.btns[0].height() - 1);
            p.drawPixmap(self.m_x + (self.btns[0].width() - self.pix.width()) / 2,
                         self.btns[0].y() + self.btns[0].height() - self.pix.height() + 1, self.pix);

        else:
            if self.isDrawVerticalLine:
                p.drawLine(self.width()-1,0,self.width()-1,self.height())
            p.drawLine(0, self.btns[0].y() + self.btns[0].height() - 1, self.index * self.btns[0].width() + (self.btns[0].width() - self.pix.width()) / 2 - 1, self.btns[0].y() + self.btns[0].height() - 1);
            p.drawLine(self.index * self.btns[0].width() + (self.btns[0].width() - self.pix.width()) / 2 + self.pix.width(), self.btns[0].y() + self.btns[0].height() - 1, self.width(), self.btns[0].y() + self.btns[0].height() - 1);
            p.drawPixmap(self.index * self.btns[0].width() + (self.btns[0].width() - self.pix.width()) / 2, self.btns[0].y() + self.btns[0].height() - self.pix.height() + 1, self.pix);


    def slot_btns(self):
        self.preindex = self.stackwid.currentIndex()
        self.stackwid.setCurrentIndex(int(self.sender().objectName()))
        
    def resizeEvent(self, e:QResizeEvent):
        super(MiddleWidgetLeft, self).resizeEvent(e)



























