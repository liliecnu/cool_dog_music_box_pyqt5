from PyQt5 import QtCore,QtGui,QtWidgets
from PyQt5.QtCore import QPoint,QRect,QPropertyAnimation
from PyQt5.QtWidgets import QWidget,QFrame,QVBoxLayout,QStackedWidget,QLabel,QLineEdit,QSizePolicy,QHBoxLayout,QSpacerItem
from PyQt5.QtGui import QPainter,QPixmap,QColor,QResizeEvent
from absFiles.BaseWidget import *
from absFiles.myPushButton import *

class MiddleWidgetRight(BaseWidget):
    def __init__(self,parent=None):
        self.stackwid = QStackedWidget(self)
        self.lrcwid = None
        self.searchwid = None
        self.btns = []
        self.mvWid = None
        self.wid = BaseWidget(self)

        self.isdrawline = True
        self.init()
        self.btns[0].clicked.emit()

    def init(self):
        self.setMinimumWidth(690)
        self.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)
        hlayout = QHBoxLayout()
        vlayout = QVBoxLayout()
        for i in range(6):
            self.btns.append(stackButton())
        self.btns[0].setText("乐库");
        self.btns[1].setText("电台");
        self.btns[2].setText("歌单");
        self.btns[3].setText("直播");
        self.btns[4].setText("MV");
        self.btns[5].setText("歌词");


        self.stackwid.addWidget(BaseWidget(self))
        self.stackwid.addWidget(BaseWidget(self))
        self.stackwid.addWidget(BaseWidget(self))
        self.stackwid.addWidget(BaseWidget(self))
        self.stackwid.addWidget(self.mvWid)
        self.stackwid.addWidget(self.lrcwid)
        self.stackwid.addWidget(self.searchwid)

        self.btns[0].setFixedSize(64,40)
        self.btns[1].setFixedSize(64, 40)
        self.btns[2].setFixedSize(64, 40)
        self.btns[3].setFixedSize(64, 40)
        self.btns[4].setFixedSize(64, 40)
        self.btns[5].setFixedSize(64, 40)

        self.btns[0].setStyleSheet("QPushButton{color:rgb(68,68,68);font-size:17px;font-family:黑体;}"
                                    "QPushButton:hover{color:rgb(40,143,231);}");
        self.btns[1].setStyleSheet("QPushButton{color:rgb(68,68,68);font-size:17px;font-family:黑体;}"
                                    "QPushButton:hover{color:rgb(40,143,231);}");
        self.btns[2].setStyleSheet("QPushButton{color:rgb(68,68,68);font-size:17px;font-family:黑体;}"
                                    "QPushButton:hover{color:rgb(40,143,231);}");
        self.btns[3].setStyleSheet("QPushButton{color:rgb(68,68,68);font-size:17px;font-family:黑体;}"
                                    "QPushButton:hover{color:rgb(40,143,231);}");
        self.btns[4].setStyleSheet("QPushButton{color:rgb(68,68,68);font-size:17px;font-family:黑体;}"
                                    "QPushButton:hover{color:rgb(40,143,231);}");
        self.btns[5].setStyleSheet("QPushButton{color:rgb(68,68,68);font-size:17px;font-family:黑体;}"
                                    "QPushButton:hover{color:rgb(40,143,231);}");
        hlayout.addStretch(60)
        hlayout.addWidget(self.btns[0], 0, QtCore.Qt.AlignHCenter)
        hlayout.addWidget(self.btns[1], 0, QtCore.Qt.AlignHCenter)
        hlayout.addWidget(self.btns[2], 0, QtCore.Qt.AlignHCenter)
        hlayout.addWidget(self.btns[3], 0, QtCore.Qt.AlignHCenter)
        hlayout.addWidget(self.btns[4], 0, QtCore.Qt.AlignHCenter)
        hlayout.addWidget(self.btns[5], 0, QtCore.Qt.AlignHCenter)

        hlayout.addStretch(60)
        hlayout.setSpacing(25)
        hlayout.setContentsMargins(0,0,0,0)

        for i in range(6):
            self.btns[i].setObjectName(str(i))
            self.btns[i].clicked.connect(self.slot_btnClick)
        self.wid.setLayout(hlayout)
        vlayout.addWidget(self.wid)
        vlayout.addWidget(self.stackwid)
        vlayout.setSpacing(0)
        self.setLayout(vlayout)
        self.stackwid.currentChanged.connect(self.slot_curStackChange)


    def slot_setSearchStack(self):
        lineedit = self.sender()
        text = lineedit.text().simplified()
        if text.isEmpty():
            text = lineedit.placeholderText()
        self.slot_search(text)
    def slot_search(self,text):
        self.searchwid.setSearchName(text.simplified())















































