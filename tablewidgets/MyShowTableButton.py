from PyQt5 import QtCore,QtGui,QtWidgets
from PyQt5.QtCore import QPoint,QRect
from PyQt5.QtWidgets import QWidget,QAction,QLabel,QLineEdit,QSizePolicy,QMenu,QPushButton,QHBoxLayout,QSpacerItem,QScrollArea,QFrame
from PyQt5.QtGui import QPainter
from absFiles.myPushButton import *



class MyShowTableButton(QPushButton):
    sig_emptyList = QtCore.pyqtSignal()
    sig_addPlayList = QtCore.pyqtSignal()
    sig_addSong = QtCore.pyqtSignal()
    sig_delPlayList = QtCore.pyqtSignal()
    sig_reName = QtCore.pyqtSignal(str)
    def __init__(self,parent=None):
        super(MyShowTableButton, self).__init__(parent)
        self.indicator = QLabel(self)
        self.menu = QMenu(self)
        self.btnmenu = MyPushButton(self)
        self.lineEdit = QLineEdit(self)
        self.playlistName = QLabel(self)
        self.playlistName.setText("新建列表[]")
        self.finalWid = None
        self.isTipsStyle = False
        self.isDrawTop = False
        self.isDrawMove = False
        self.presspos = QPoint()
        self.setAcceptDrops(True)
        self.init()
        self.initMenu()
        self.setFixedHeight(40)

    def initMenu(self):
        act_addplaylist = QAction("新建列表")
        act_addsong = QAction("添加歌曲")
        act_delplayList = QAction("删除列表")
        act_reName = QAction("重命名")
        act_emptyList = QAction("清空列表")

        self.menu.addAction(act_addplaylist)
        self.menu.addSeparator()
        self.menu.addAction(act_addsong)
        self.menu.addAction(QAction("稍后播"));
        self.menu.addAction(QAction("添加到播放列表"));
        self.menu.addAction(QAction("全部发送到移动设备"));
        self.menu.addAction(QAction("下载本列表全部歌曲"));
        self.menu.addAction(QAction("排序"));
        self.menu.addAction(QAction("匹配MV"));
        self.menu.addAction(QAction("收藏整个列表"));
        self.menu.addSeparator();
        self.menu.addAction(act_emptyList);
        self.menu.addAction(act_delplayList);
        self.menu.addAction(act_reName);
        self.menu.setContentsMargins(4, 10, 3, 10);
        self.menu.actionGeometry(act_reName);

        act_emptyList.triggered.connect(self.sig_emptyList)
        act_reName.triggered.connect(self.slot_ReName)
        act_delplayList.triggered.connect(self.sig_delPlayList)
        act_addplaylist.triggered.connect(self.sig_addPlayList)
        act_addsong.triggered.connect(self.sig_addSong)
        act_emptyList.triggered.connect(self.sig_emptyList)
        self.sig_reName.connect(self.slot_reNameDB)


