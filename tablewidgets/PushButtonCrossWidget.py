from PyQt5 import QtCore,QtGui,QtWidgets
from PyQt5.QtCore import QPoint,QRect
from PyQt5.QtWidgets import QWidget,QLabel,QLineEdit,QSizePolicy,QHBoxLayout,QVBoxLayout,QSpacerItem
from PyQt5.QtGui import QPainter
from absFiles.myPushButton import *


class PushButtonCrossWidget(QWidget):
    def __init__(self,parent=None):
        super(PushButtonCrossWidget, self).__init__(parent)
        self.btn = MyPushButton(self)
        self.btn.setFixedSize(16,16)
        self.btn.setStyleSheet("QPushButton{border-image:url(:/image/middlewidget/btn_cross (1).png);}"
                         "QPushButton:hover{border-image:url(:/image/middlewidget/btn_cross (2).png);}"
                         "QPushButton:pressed{border-image:url(:/image/middlewidget/btn_cross (3).png);}")


        vlayout = QVBoxLayout()
        vlayout.addWidget(self.btn,0,QtCore.Qt.AlignCenter)
        vlayout.setContentsMargins(0,0,0,0)
        self.setLayout(vlayout)


class PushButtonGroupWidget(QWidget):
    def __init__(self,parent=None):
        super(PushButtonGroupWidget, self).__init__(parent)
        self.btnMovie = MyPushButton(self)
        self.btnLove = MyPushButton(self)
        self.btnDel = MyPushButton(self)
        self.btnMore = MyPushButton(self)
        self.islove = False
        hlayout = QHBoxLayout()
        self.btnLove.setFixedSize(16, 16);

        self.btnDel.setFixedSize(16, 16);

        self.btnMore.setFixedSize(16, 16);

        self.btnMovie.setFixedSize(16, 16);

        self.btnMovie.setStyleSheet("QPushButton{border:NULL;background-image:url(:/image/middlewidget/btn_mv (2).png);}"
                                 "QPushButton:hover{border:NULL;background-image:url(:/image/middlewidget/btn_mv (1).png);}"
                                 "QPushButton:pressed{border:NULL;background-image:url(:/image/middlewidget/btn_mv (3).png);}");

        self.btnLove.setStyleSheet("QPushButton{border-image:url(:/image/middlewidget/btn_love (1).png);}"
                                "QPushButton:hover{border-image:url(:/image/middlewidget/btn_love (2).png);}"
                                "QPushButton:pressed{border-image:url(:/image/middlewidget/btn_love (3).png);}");

        self.btnDel.setStyleSheet("QPushButton{border-image:url(:/image/middlewidget/btn_del (1).png);}"
                               "QPushButton:hover{border-image:url(:/image/middlewidget/btn_del (2).png);}"
                               "QPushButton:pressed{border-image:url(:/image/middlewidget/btn_del (3).png);}");

        self.btnMore.setStyleSheet("QPushButton{border-image:url(:/image/middlewidget/btn_more (1).png);}"
                                "QPushButton:hover{border-image:url(:/image/middlewidget/btn_more (2).png);}"
                                "QPushButton:pressed{border-image:url(:/image/middlewidget/btn_more (3).png);}");

        hlayout.addWidget(self.btnMovie, 0, QtCore.Qt.AlignCenter);
        hlayout.addWidget(self.btnLove, 0, QtCore.Qt.AlignCenter);
        hlayout.addWidget(self.btnDel, 0, QtCore.Qt.AlignCenter);
        hlayout.addWidget(self.btnMore, 0, QtCore.Qt.AlignCenter);
        hlayout.addSpacing(14);
        hlayout.setSpacing(5);
        hlayout.setContentsMargins(0, 0, 0, 0);
        self.setLayout(hlayout);

        self.btnLove.clicked.connect(self.slot_btnloveclicked)

    def setLoved(self):
        self.islove = True
        self.btnLove.setStyleSheet("QPushButton{border-image:url(:/image/middlewidget/btn_islove (1).png);}"
                                         "QPushButton:hover{border-image:url(:/image/middlewidget/btn_islove (2).png);}"
                                         "QPushButton:pressed{border-image:url(:/image/middlewidget/btn_islove (3).png);}")

    def slot_btnloveclicked(self):
        if self.islove:
            self.btnLove.setStyleSheet("QPushButton{border-image:url(:/image/middlewidget/btn_love (1).png);}"
                                 "QPushButton:hover{border-image:url(:/image/middlewidget/btn_love (2).png);}"
                                 "QPushButton:pressed{border-image:url(:/image/middlewidget/btn_love (3).png);}")
        else:
            self.btnLove.setStyleSheet("QPushButton{border-image:url(:/image/middlewidget/btn_islove (1).png);}"
                                    "QPushButton:hover{border-image:url(:/image/middlewidget/btn_islove (2).png);}"
                                    "QPushButton:pressed{border-image:url(:/image/middlewidget/btn_islove (3).png);}");
        self.islove = not self.islove





























