from PyQt5 import QtCore,QtGui,QtWidgets
from PyQt5.QtCore import QPoint,QRect
from PyQt5.QtWidgets import QWidget,QLabel,QLineEdit,QSizePolicy,QHBoxLayout,QSpacerItem
from PyQt5.QtGui import QPainter
from absFiles.myPushButton import *
from res.res_bottomwidget import *
from res.res_main import *
from res.res_middlewidget import *
from res.res_otherwidget import *
from res.res_topwidget import *
from res.res_traymenu import *

class TopWidgets(QWidget):
    def __init__(self,parent=None):
        super(TopWidgets, self).__init__(parent)
        self.btnexit = MyPushButton()
        self.btnmini = MyPushButton()
        self.btnminiframe = MyPushButton()
        self.btnmobile = MyPushButton()
        self.btnsetting = MyPushButton()
        self.btnskin = MyPushButton()
        self.btnmessage = MyPushButton()
        self.btnplaycard = MyPushButton()
        self.btntool = MyPushButton()
        self.btngame = MyPushButton()
        self.lineEdit = QLineEdit()
        self.btnlog = MyPushButton(self)
        self.btnlog.setText("登陆")
        self.btnregister = MyPushButton(self)
        self.btnregister.setText("注册")
        self.setStyleSheet("TopWidgets{background:transparent;}")
        self.setMouseTracking(True)
        self.initWidgets()

    def slot_serTipsClick(self,item):
        self.searchFromLineEdit(item.text())
    def initWidgets(self):
        self.setMinimumSize(1000, 50)
        self.setMaximumSize(1920, 50)
        self.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Fixed)
        hmainyout = QHBoxLayout()
        hyout1 = QHBoxLayout()
        
        btn = MyPushButton(self);
        btn.setFixedSize(30, 30);
        btn.setStyleSheet("QPushButton{background:transparent;border-image:url(:/image/topwidget/label_icon.png)}");

        self.btnlog.setFixedSize(26, 24);
        self.btnregister.setFixedSize(26, 24);
        self.btnlog.setStyleSheet("QPushButton{background:transparent;color:white;font-family:宋体;font-size:12px;}");
        self.btnregister.setStyleSheet("QPushButton{background:transparent;color:white;font-family:宋体;font-size:12px;}");
        label2 = QLabel("丨", self);

        label2.setFixedSize(10, 20);
        label2.setStyleSheet("color:white;");
        label2.adjustSize();
        hyout1.addWidget(btn);
        hyout1.addSpacing(6);
        hyout1.addWidget(self.btnlog);
        hyout1.addWidget(label2);
        hyout1.addWidget(self.btnregister);
        hyout1.setSpacing(5);
        hyout1.setContentsMargins(0, 0, 0, 0);

        hyout2 = QHBoxLayout()
        self.btnreturn = MyPushButton(self);
        self.btnreturn.setFixedSize(20, 20);
        self.btnreturn.setStyleSheet("QPushButton{border-image:url(:/image/topwidget/btn_ return(1).png);}"
                                   "QPushButton::hover{border-image:url(:/image/topwidget/btn_ return(2).png);}"
                                   "QPushButton::pressed{border-image:url(:/image/topwidget/btn_ return(3).png);}");
        self.btnrefresh = MyPushButton(self);
        self.btnrefresh.setFixedSize(20, 20);
        self.btnrefresh.setStyleSheet("QPushButton{border-image:url(:/image/topwidget/btn_refresh(1).png);}"
                                    "QPushButton::hover{border-image:url(:/image/topwidget/btn_refresh(2).png);}"
                                    "QPushButton::pressed{border-image:url(:/image/topwidget/btn_refresh(3).png);}");

        self.lineEdit.setMinimumSize(260, 26);
        self.lineEdit.setMaximumSize(320, 26);
        self.lineEdit.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed);
        self.lineEdit.setStyleSheet("QLineEdit{border-radius:13px;background:rgb(255,255,255,180);}");
        self.lineEdit.setContextMenuPolicy(QtCore.Qt.NoContextMenu);
        serbtn = MyPushButton(self.lineEdit)
        serbtn.setFixedSize(16, 16);
        serbtn.setStyleSheet(
            "QPushButton{background:transparent;border-image: url(:/image/topwidget/btn_search (1).png);}"
            "QPushButton::hover{border-image: url(:/image/topwidget/btn_search (2).png);}"
            "QPushButton::pressed{border-image: url(:/image/topwidget/btn_search (3).png);}");
        self.lineEdit.setPlaceholderText("周杰伦-东风破");
        margins = self.lineEdit.textMargins();
        self.lineEdit.setTextMargins(margins.left() + 15, margins.top(), serbtn.width() + 15, margins.bottom());


        pSearchLayout = QHBoxLayout(self);
        pSearchLayout.addStretch();
        pSearchLayout.addWidget(serbtn);
        pSearchLayout.setSpacing(0);
        pSearchLayout.setContentsMargins(0, 0, 15, 0);
        self.lineEdit.setLayout(pSearchLayout);

        hyout2.addWidget(self.btnreturn);
        hyout2.addWidget(self.btnrefresh);
        hyout2.addWidget(self.lineEdit);
        hyout2.setSpacing(20);
        hyout2.setContentsMargins(0, 0, 0, 0);

        hyout3 = QHBoxLayout()

        self.btnexit.setFixedSize(18, 18);
        self.btnmini.setFixedSize(16, 16);
        self.btnminiframe.setFixedSize(16, 16);
        self.btnmobile.setFixedSize(16, 16);
        self.btnsetting.setFixedSize(16, 16);
        self.btnskin.setFixedSize(16, 16);
        self.btnmessage.setFixedSize(16, 16);
        self.btnplaycard.setFixedSize(16, 16);
        self.btntool.setFixedSize(16, 16);
        self.btngame.setFixedSize(16, 16);

        self.btnexit.setObjectName("self.btnexit");
        self.btnmini.setObjectName("self.btnmini");
        self.btnminiframe.setObjectName("self.btnminiframe");
        self.btnmobile.setObjectName("self.btnmobile");
        self.btnsetting.setObjectName("self.btnsetting");
        self.btnskin.setObjectName("self.btnskin");
        self.btnmessage.setObjectName("self.btnmessage");
        self.btnplaycard.setObjectName("self.btnplaycard");
        self.btntool.setObjectName("self.btntool");
        self.btngame.setObjectName("self.btngame");
        self.btnexit.setStyleSheet("QPushButton{border-image:url(:/image/topwidget/btn_close (1).png);}"
                                "QPushButton::hover{border-image:url(:/image/topwidget/btn_close (2).png);}"
                                "QPushButton::pressed{border-image:url(:/image/topwidget/btn_close (3).png);}");
        self.btnmini.setStyleSheet("QPushButton{border-image:url(:/image/topwidget/btn_mini (1).png);}"
                                "QPushButton::hover{border-image:url(:/image/topwidget/btn_mini (2).png);}"
                                "QPushButton::pressed{border-image:url(:/image/topwidget/btn_mini (3).png);}");
        self.btnminiframe.setStyleSheet("QPushButton{border-image:url(:/image/topwidget/btn_miniframe (1).png);}"
                                     "QPushButton::hover{border-image:url(:/image/topwidget/btn_miniframe (2).png);}"
                                     "QPushButton::pressed{border-image:url(:/image/topwidget/btn_miniframe (3).png);}");
        self.btnmobile.setStyleSheet("QPushButton{border-image:url(:/image/topwidget/btn_mobile (1).png);}"
                                  "QPushButton::hover{border-image:url(:/image/topwidget/btn_mobile (2).png);}"
                                  "QPushButton::pressed{border-image:url(:/image/topwidget/btn_mobile (3).png);}");
        self.btnsetting.setStyleSheet("QPushButton{border-image:url(:/image/topwidget/btn_setting (1).png);}"
                                   "QPushButton::hover{border-image:url(:/image/topwidget/btn_setting (2).png);}"
                                   "QPushButton::pressed{border-image:url(:/image/topwidget/btn_setting (3).png);}");
        self.btnskin.setStyleSheet("QPushButton{border-image:url(:/image/topwidget/btn_skin (1).png);}"
                                "QPushButton::hover{border-image:url(:/image/topwidget/btn_skin (2).png);}"
                                "QPushButton::pressed{border-image:url(:/image/topwidget/btn_skin (3).png);}");
        self.btnmessage.setStyleSheet("QPushButton{border-image:url(:/image/topwidget/btn_message (1).png);}"
                                   "QPushButton::hover{border-image:url(:/image/topwidget/btn_message (2).png);}"
                                   "QPushButton::pressed{border-image:url(:/image/topwidget/btn_message (3).png);}");
        self.btnplaycard.setStyleSheet("QPushButton{border-image:url(:/image/topwidget/btn_playcard (1).png);}"
                                    "QPushButton::hover{border-image:url(:/image/topwidget/btn_playcard (2).png);}"
                                    "QPushButton::pressed{border-image:url(:/image/topwidget/btn_playcard (3).png);}");
        self.btntool.setStyleSheet("QPushButton{border-image:url(:/image/topwidget/btn_tool (1).png);}"
                                "QPushButton::hover{border-image:url(:/image/topwidget/btn_tool (2).png);}"
                                "QPushButton::pressed{border-image:url(:/image/topwidget/btn_tool (3).png);}");

        self.btngame.setStyleSheet("QPushButton{border-image:url(:/image/topwidget/btn_game (1).png);}"
                            "QPushButton::hover{border-image:url(:/image/topwidget/btn_game (2).png);}"
                            "QPushButton::pressed{border-image:url(:/image/topwidget/btn_game (3).png);}");

        label3 = QLabel("丨", self);
        label3.setFixedSize(6, 16);
        label3.setStyleSheet("color:rgb(255,255,255,50);");
        label3.adjustSize();
    
        hyout3.addWidget(self.btntool);
        hyout3.addWidget(self.btnplaycard);
        hyout3.addWidget(self.btngame);
        hyout3.addWidget(self.btnmessage);
    
        hyout3.addWidget(self.btnskin);
        hyout3.addWidget(self.btnsetting);
        hyout3.addWidget(label3);
        hyout3.addWidget(self.btnmobile);
        hyout3.addWidget(self.btnminiframe);
        hyout3.addWidget(self.btnmini);
        hyout3.addWidget(self.btnexit);
        hyout3.setSpacing(12);
    
        hmainyout.addLayout(hyout1);
        hmainyout.addSpacing(190);
        hmainyout.addSpacerItem(QSpacerItem(80, 30, QSizePolicy.Maximum));
        hmainyout.addLayout(hyout2);
        hmainyout.addSpacerItem(QSpacerItem(110, 30, QSizePolicy.Expanding));
        hmainyout.addLayout(hyout3);
        hmainyout.setSpacing(0);
        hmainyout.setContentsMargins(16, 0, 15, 0);
        self.setLayout(hmainyout);

        serbtn.clicked.connect(self.lineEdit.returnPressed)












