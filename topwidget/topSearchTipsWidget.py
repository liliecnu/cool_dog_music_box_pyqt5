from PyQt5 import QtCore, QtGui, QtWidgets,QtNetwork
from PyQt5.QtCore import QPoint, QRect,QUrl,QEventLoop
from PyQt5.QtWidgets import QWidget, QStyleOption, QStyle, QListWidget, QListWidgetItem,QFrame,QAbstractItemView
from PyQt5.QtGui import QPainter,QCursor,QColor
from PyQt5.QtNetwork import QNetworkAccessManager,QNetworkRequest,QNetworkReply
import json
class TopSearchTipsWidget(QListWidget):
    def __init__(self,parent=None):
        super(TopSearchTipsWidget, self).__init__(parent)
        self.manager = QNetworkAccessManager(self)
        self.setMouseTracking(True);
        self.setStyleSheet("QListWidget{background:white;border:1px solid rgb(214,214,214);}"
                      "QListWidget.item{color:rgb(102,102,102);height:30;}");
        self.setFrameShadow(QFrame.Plain);
        self.setFrameShape(QFrame.NoFrame);
        self.setFocusPolicy(QtCore.Qt.NoFocus);
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff);
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff);
        self.setSelectionBehavior(QAbstractItemView.SelectRows);
        self.setSelectionMode(QAbstractItemView.NoSelection);
        self.prerow = -1;

    def mouseMoveEvent(self, e: QtGui.QMouseEvent):
        super(TopSearchTipsWidget, self).mouseMoveEvent(e)
        listitem = self.itemAt(self.mapFromGlobal(QCursor.pos()))
        if listitem == None:
            for i in range(self.count()):
                self.item(i).setBackground(QtCore.Qt.transparent)
        else:
            plistitem = self.item(self.prerow)
            if plistitem != None:
                plistitem.setBackground(QtCore.Qt.transparent)
            plistitem.setBackground(QColor(244,244,244))
            self.prerow = self.row(listitem)

    def leaveEvent(self, a0: QtCore.QEvent):
        super(TopSearchTipsWidget, self).leaveEvent(a0)
        listitem = self.item(self.prerow)
        if listitem != None:
            listitem.setBackground(QtCore.Qt.transparent)
    def focusInEvent(self, e: QtGui.QFocusEvent):
        super(TopSearchTipsWidget, self).focusInEvent(e)
    def slot_textchanged(self,str):
        self.clear()
        count = self.count()
        for i in range(count):
            self.takeItem(0)

        request =QtNetwork.QNetworkRequest()
        request.setUrl(QUrl("http://i.y.qq.com/s.plcloud/fcgi-bin/smartbox_new.fcg?key=" + str + "&utf8=1&is_json=1"));
        request.setRawHeader("Accept", "*/*");
        request.setRawHeader("Accept-Language", "zh-CN");
        request.setRawHeader("User-Agent", "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)");
        request.setRawHeader("Host", "i.y.qq.com");
        request.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
        request.setRawHeader("Accept-Encoding", "deflate");
        request.setRawHeader("Cookie", "qqmusic_fromtag=3; qqmusic_miniversion=57; qqmusic_version=12;");
        reply = self.manager.get(request)
        loop = QEventLoop()
        reply.finished.connect(loop.quit)
        loop.exec()
        if(reply.error() == QNetworkReply.NoError):
            byt = reply.readAll()
            json_str = json.dumps(byt)
            objtemp = json_str["data"]
            objtemp = objtemp["song"]
            objtemp = objtemp["itemlist"]
            for item in objtemp:
                songname = objtemp["name"]
                songsinger = objtemp["singer"]
                item = QListWidgetItem(songsinger+"-"+songname)
                self.addItem(item)
        reply.deleteLater()












